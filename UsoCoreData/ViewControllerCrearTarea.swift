//
//  ViewControllerCrearTarea.swift
//  UsoCoreData
//
//  Created by Gonzalo Quispe Fernandez on 5/28/20.
//  Copyright © 2020 Gonzalo Quispe Fernandez. All rights reserved.
//

import UIKit

class ViewControllerCrearTarea: UIViewController {

    @IBOutlet weak var txtNombreTarea: UITextField!
    
    @IBOutlet weak var swImportante: UISwitch!
    
    var anteriorVC = ViewController()

    @IBAction func agregar(_ sender: Any) {
        
        //let tarea = Tarea()
        
        //Se apertura uso de persistencia
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let tarea  = Tarea(context: context)
        tarea.nombre = txtNombreTarea.text!
        tarea.importante = swImportante.isOn
        
        //anteriorVC.tareas.append(tarea)
        //anteriorVC.tableView.reloadData()
        navigationController?.popViewController(animated: true)
        
    }
    
    /*func saveContext(){
        let context = persistentContainer.viewContext
        if context.hasChanges{
            do{
                try context.save()
            }catch{
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
