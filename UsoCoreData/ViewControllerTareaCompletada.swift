//
//  ViewControllerTareaCompletada.swift
//  UsoCoreData
//
//  Created by Gonzalo Quispe Fernandez on 5/28/20.
//  Copyright © 2020 Gonzalo Quispe Fernandez. All rights reserved.
//

import UIKit

class ViewControllerTareaCompletada: UIViewController {

    
    var tarea:Tarea? = nil
    
    //var anteriorVC = ViewController()
    
    @IBOutlet weak var tareaLabel: UILabel!
    
    @IBAction func completarTarea(_ sender: Any) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        context.delete(tarea!)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if tarea!.importante{
            tareaLabel.text = "😇\(tarea!.nombre!)"
        }else{
            tareaLabel.text = tarea!.nombre!
        }
        
    }
    

    
}
